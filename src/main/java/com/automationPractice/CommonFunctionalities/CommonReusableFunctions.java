package com.automationPractice.CommonFunctionalities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonReusableFunctions 
{
	public WebDriver driver;
	public WebDriverWait wait;
	public CommonReusableFunctions(WebDriver driver) 
	{
		this.driver=driver;
		wait=new WebDriverWait(this.driver, 120);
	}
	
	public String getTimeStamp()
	{
		String timeStamp = new SimpleDateFormat("ddMMYYYYHHmmss").format(new Date());
		return timeStamp;
	}
	
	public void captureScreenshot(String elementName) throws IOException
	{
		File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileHandler.copy(src, new File(".\\Screenshot\\Failed"+elementName+getTimeStamp()+".png"));
	}
	public void elementClick(WebElement element, String elementName, String pageName) throws Exception
	{
		wait.until(ExpectedConditions.visibilityOf(element));
		if(element.isEnabled()) 
		{
			element.click();
		}
		else
		{
			captureScreenshot(elementName);
			throw new Exception(elementName+" is not displayed/enabled in "+pageName);
		}
	}
	public void elementClick(By element, String elementName, String pageName) throws Exception
	{
		WebElement webElement = driver.findElement(element);
		wait.until(ExpectedConditions.visibilityOf(webElement));
		try
		{
			webElement.click();
		}
		catch(Exception e)
		{
			captureScreenshot(elementName);
			throw new Exception(elementName+" is not displayed/enabled in "+pageName);
		}
	}
	public void enterTextValue(WebElement element, String elementName, String pageName, String text) throws Exception
	{
		wait.until(ExpectedConditions.visibilityOf(element));
		try
		{
			element.click();
			element.clear();
			element.sendKeys(text);
		}
		catch(Exception e)
		{
			captureScreenshot(elementName);
			throw new Exception(elementName+" is not displayed/enabled in "+pageName);
		}
	}
	public void enterTextValue(By element, String elementName, String pageName, String text) throws Exception
	{
		WebElement webElement = driver.findElement(element);
		wait.until(ExpectedConditions.visibilityOf(webElement));
		try
		{
			webElement.click();
			webElement.clear();
			webElement.sendKeys(text);
		}
		catch(Exception e)
		{
			captureScreenshot(elementName);
			throw new Exception(elementName+" is not displayed/enabled in "+pageName);
		}
	}
	public void selectValueFromDropdown(WebElement element, String elementName, String pageName, String option) throws Exception
	{
		try
		{
			Select select = new Select(element);
			boolean flag = verifyOptionPresent(element, option);
			if(flag)
				select.selectByVisibleText(option);
			else
				throw new Exception(option+" is not a valid option in "+elementName);
		}
		catch(Exception e)
		{
			captureScreenshot(elementName);
			throw new Exception(elementName+" is not displayed/enabled in "+pageName);
		}
	}
	public boolean verifyOptionPresent(WebElement element, String option)
	{
		boolean flag = false;
		Select select = new Select(element);
		List<WebElement> options = select.getOptions();
		for (WebElement x : options) {
			if (x.getText().equals(option)) {
				flag = true;
				break;
			}
		}

		return flag;
	}
	
	public Map<String, Integer> getColIndex(List<WebElement> tableHeaders) throws Exception
	{
		Map<String, Integer> colNameIndex = new HashMap<String, Integer>();
		for(int i=0; i<tableHeaders.size(); i++)
		{
			String actualColumnHeader = tableHeaders.get(i).getText();
			colNameIndex.put(actualColumnHeader, i+1);
		}
		if(colNameIndex!=null)
			return colNameIndex;
		else
			throw new Exception("Desired Table has no table headers");
	}
	
	public int getRowIndex(List<WebElement> tableItems, String desiredItem) throws Exception
	{
		int rowIndex = 0;
		for(int i=0; i<tableItems.size(); i++)
		{
			String actualItemDescription = tableItems.get(i).getText();
			if(actualItemDescription.equals(desiredItem))
			{
				rowIndex = i+1;
				break;
			}
		}
		if(rowIndex!=0)
			return rowIndex;
		else
			throw new Exception("Desired Item is not present in cart table");
	}
	
	public String randomString(int n) {
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) 
		{
			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (AlphaNumericString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString.charAt(index));
		}
        return sb.toString(); 
	}
	public void mouseHoverOperation(WebElement element, String elementName, String pageName) throws Exception
	{
		wait.until(ExpectedConditions.visibilityOf(element));
		if(element.isDisplayed())
		{
			Point location = element.getLocation();
			int x = location.getX();
			int y = location.getY()-300;
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("window.scrollBy("+x+","+y+")");
			Actions actions = new Actions(driver);
			actions.moveToElement(element).perform();
		}
		else
		{
			captureScreenshot(elementName);
			throw new Exception(elementName+" is not displayed/enabled in "+pageName);
		}
	}
	public void jsClick(WebElement element)
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click()", element);
	}

}
