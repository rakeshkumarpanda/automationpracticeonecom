package com.automationPractice.ObjectRepos;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WomenPageElements 
{
	@FindBy(xpath = "//h5[@itemprop='name']/a")
	public List<WebElement> womenTabItems;
	
	@FindBy(xpath = "//a[@title='Proceed to checkout']")
	public WebElement proceedToCheckoutButton;
	
	@FindBy(xpath = "//b[text()='Cart']")
	public WebElement cartIcon;
	
	@FindBy(id = "cart_title")
	public WebElement cartTitle;
	
	@FindBy(xpath = "//span[@class='cat-name']")
	public WebElement womenTabHeader;
	
	public String womenTabProductXpath = "//a[@title='Desired Product']/ancestor::div[@class='right-block']//a[@title='Add to cart']";
}
