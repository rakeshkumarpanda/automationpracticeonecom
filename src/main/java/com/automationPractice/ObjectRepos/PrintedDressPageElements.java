package com.automationPractice.ObjectRepos;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PrintedDressPageElements 
{
	@FindBy(xpath = "//h1[text()='Printed Dress']")
	public WebElement headerPrintedDress;
	
	@FindBy(id = "quantity_wanted")
	public WebElement desiredQuatityField;
	
	@FindBy(id = "group_1")
	public WebElement dressSizeDropDown;
	
	@FindBy(name = "Pink")
	public WebElement pinkColouredDress;
	
	@FindBy(xpath = "//span[text()='Add to cart']")
	public WebElement addToCartButton;

}
