package com.automationPractice.ObjectRepos;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPageElements 
{
	@FindBy(xpath = "//table[@id='cart_summary']//th")
	public List<WebElement> cartTableHeaders;
	
	@FindBy(xpath = "//p[@class='cart_navigation clearfix']/a[@title='Proceed to checkout']")
	public WebElement proceedToCheckoutCartSummary;
	
	@FindBy(name = "processAddress")
	public WebElement processAddressButton;
	
	@FindBy(name = "cgv")
	public WebElement termsConditionsCheckbox;
	
	@FindBy(name = "processCarrier")
	public WebElement proceedToCheckoutShippingTab;
	
	@FindBy(xpath = "//a[@title='Pay by bank wire']")
	public WebElement paymentByBankWire;
	
	@FindBy(xpath = "//span[contains(text(),'I confirm my order')]")
	public WebElement confirmOrderButton;
	
	@FindBy(xpath = "//span[@class='price']/strong")
	public WebElement paidAmount;
	
	@FindBy(xpath = "//div[@class='box']")
	public WebElement confirmationMessage;
	
	@FindBy(xpath = "//a[@title='View my customer account']")
	public WebElement viewCustomerAccountLink;
	
	@FindBy(xpath = "//span[text()='Order history and details']")
	public WebElement orderHistoryDetailsButton;
	
	@FindBy(xpath = "//table[@id='order-list']//th")
	public List<WebElement> orderHistoryTableHeaders;
	
	@FindBy(className = "logout")
	public WebElement logoutButton;
	
	public String cartTableDescriptionsXpath = "//table[@id='cart_summary']/tbody/tr/td[colIndex]/p/a";
	public String cartPageQuantityField = "//table[@id='cart_summary']/tbody/tr[rowIndex]/td[colIndex]/input[@type='text']";
	public String orderHistoryTable_OrderReferenceXpath = "//table[@id='order-list']/tbody/tr/td[colIndex]/a";
	public String orderHistoryTableDataXpath = "//table[@id='order-list']/tbody/tr[rowIndex]/td[colIndex]/span";
	
	public List<WebElement> replacedCartTableDescriptionsXpath(WebDriver driver, String arg)
	{
		cartTableDescriptionsXpath=cartTableDescriptionsXpath.replace("colIndex", arg);
		return driver.findElements(By.xpath(cartTableDescriptionsXpath));
	}
	public List<WebElement> replacedOrderReferenceNumberXpath(WebDriver driver, String arg)
	{
		orderHistoryTable_OrderReferenceXpath=orderHistoryTable_OrderReferenceXpath.replace("colIndex", arg);
		return driver.findElements(By.xpath(orderHistoryTable_OrderReferenceXpath));
	}
	public WebElement getAmountFromOrderHistoryTable(WebDriver driver, String rowIndex, String colIndex)
	{
		orderHistoryTableDataXpath=orderHistoryTableDataXpath.replace("rowIndex", rowIndex)
															 .replace("colIndex", colIndex);
		return driver.findElement(By.xpath(orderHistoryTableDataXpath));
	}

}
