package com.automationPractice.ObjectRepos;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EveningDressPageElements {
	
	@FindBy(xpath = "//h5[@itemprop='name']/a[contains(text(),'Printed Dress')]")
	public WebElement printedDressLink;
	
	@FindBy(xpath = "//span[text()='More']")
	public WebElement moreButton;

}
