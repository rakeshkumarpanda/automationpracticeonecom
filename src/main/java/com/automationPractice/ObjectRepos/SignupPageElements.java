package com.automationPractice.ObjectRepos;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignupPageElements 
{
	@FindBy(xpath = "//h3[text()='Your personal information']")
	public WebElement personalInfoHeader;
	
	@FindBy(xpath = "//div[contains(@class,'required')]/input")
	public List<WebElement> persoalInfoMandatoryTextFields;
	
	@FindBy(xpath = "//p[contains(@class,'required')]/input")
	public List<WebElement> addressInfoMandatoryTextFields;
	
	@FindBy(xpath = "//p[contains(@class,'required')]//select")
	public List<WebElement> addressInfoMandatoryDropdowns;
	
	@FindBy(id = "submitAccount")
	public WebElement registerButton;

}
