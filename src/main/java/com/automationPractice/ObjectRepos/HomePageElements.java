package com.automationPractice.ObjectRepos;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePageElements 
{
	@FindBy(xpath = "//div[@id='block_top_menu']/ul/li/a[text()='Dresses']")
	public WebElement dressesTab;
	
	@FindBy(xpath = "//li[a[text()='Dresses']]/ul[contains(@class,'submenu')]//a[text()='Evening Dresses']")
	public WebElement eveningDressesLink;
	
	@FindBy(xpath = "//div[@id='block_top_menu']/ul/li/a[text()='Women']")
	public WebElement womenTab;	

}
