package com.automationPractice.ObjectRepos;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConfirmationWindowElements 
{
	@FindBy(xpath = "//span[@title='Continue shopping']")
	public WebElement continueShoppingButton;

}
