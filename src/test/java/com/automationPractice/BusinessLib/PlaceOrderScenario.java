package com.automationPractice.BusinessLib;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.automationPractice.CommonFunctionalities.CommonReusableFunctions;
import com.automationPractice.ObjectRepos.CartPageElements;
import com.automationPractice.ObjectRepos.ConfirmationWindowElements;
import com.automationPractice.ObjectRepos.EveningDressPageElements;
import com.automationPractice.ObjectRepos.HomePageElements;
import com.automationPractice.ObjectRepos.LoginPageElements;
import com.automationPractice.ObjectRepos.PrintedDressPageElements;
import com.automationPractice.ObjectRepos.SignupPageElements;
import com.automationPractice.ObjectRepos.WomenPageElements;

public class PlaceOrderScenario extends CommonReusableFunctions {
	WebDriver driver;
	HomePageElements homePage;
	EveningDressPageElements eveningDressPage;
	PrintedDressPageElements printedDressPage;
	ConfirmationWindowElements confirmationWindow;
	WomenPageElements womenPage;
	CartPageElements cartPage;
	LoginPageElements loginPage;
	SignupPageElements signupPage;

	public PlaceOrderScenario(WebDriver driver) {
		super(driver);
		this.driver = driver;
		homePage = PageFactory.initElements(this.driver, HomePageElements.class);
		eveningDressPage = PageFactory.initElements(driver, EveningDressPageElements.class);
		printedDressPage = PageFactory.initElements(driver, PrintedDressPageElements.class);
		confirmationWindow = PageFactory.initElements(driver, ConfirmationWindowElements.class);
		womenPage = PageFactory.initElements(driver, WomenPageElements.class);
		cartPage = PageFactory.initElements(driver, CartPageElements.class);
		loginPage = PageFactory.initElements(driver, LoginPageElements.class);
		signupPage = PageFactory.initElements(driver, SignupPageElements.class);
	}

	public void placeOrderScenario(Map<String, String> testData) throws Exception {
		
		// Home Page Elements
		// Clicking on Eveing Dresses tab on the home page after hovering on Dresses tab
		mouseHoverOperation(homePage.dressesTab, "Dress Tab", "Home Page");
		elementClick(homePage.eveningDressesLink, "Evening Dresses", "Home Page");

		// Evening Dress Page Elements
		mouseHoverOperation(eveningDressPage.printedDressLink, "Printed Dress", "Evening Dress Page");
		elementClick(eveningDressPage.moreButton, "More Button", "Evening Dress Page");

		// Printed Dress Page Elements
		wait.until(ExpectedConditions.visibilityOf(printedDressPage.headerPrintedDress));
		enterTextValue(printedDressPage.desiredQuatityField, "Quantity Field", "Printed Dress Page", testData.get("InitialQuantity"));
		selectValueFromDropdown(printedDressPage.dressSizeDropDown, "Dress Size", "Printed Dress Page", testData.get("InitialDressSize"));
		elementClick(printedDressPage.pinkColouredDress, "Pink Colour", "Printed Dress Page");
		elementClick(printedDressPage.addToCartButton, "Add to Cart Button", "Printed Dress Page");

		// Confirmation Window
		elementClick(confirmationWindow.continueShoppingButton, "Continue Shopping Button", "Confirmation Window");

		// Home Page Elements
		elementClick(homePage.womenTab, "Women Tab", "Home Page");

		// Women Tab Page Elements
		wait.until(ExpectedConditions.visibilityOf(womenPage.womenTabHeader));

		String desiredProductWomenTab = testData.get("DesiredProductWomenTab");
		By desiredProductWomenTabXpath = By.xpath(womenPage.womenTabProductXpath.replace("Desired Product", desiredProductWomenTab));
		boolean flag = false;
		for (WebElement x : womenPage.womenTabItems) {
			if (x.getText().equals(desiredProductWomenTab)) {
				flag = true;
				mouseHoverOperation(x, "Printed Chiffon Dress", "Women Dress Page");
				break;
			}
		}
		if (flag) {
			elementClick(desiredProductWomenTabXpath, desiredProductWomenTab, "Women Page");
			elementClick(womenPage.proceedToCheckoutButton, "Proceed to Checkout Button", "Women Page");
		} else
			elementClick(womenPage.cartIcon, "Cart Icon", "Women Page");
		wait.until(ExpectedConditions.visibilityOf(womenPage.cartTitle));

		// Cart Page Elements
		//Cart Summary
		Map<String, Integer> headerColIndices = getColIndex(cartPage.cartTableHeaders);
		List<WebElement> descriptionRowIndex=cartPage.replacedCartTableDescriptionsXpath(driver, Integer.toString(headerColIndices.get("Description")));
		int desiredRowIndex = getRowIndex(descriptionRowIndex, "Printed Dress");
		String desiredQuantityField = cartPage.cartPageQuantityField.replace("rowIndex", Integer.toString(desiredRowIndex))
				.replace("colIndex", Integer.toString(headerColIndices.get("Qty")));
		By desiredQuantityFieldXpath = By.xpath(desiredQuantityField);
		enterTextValue(desiredQuantityFieldXpath, "Desired Quantity Field", "Cart Page", testData.get("UpdatedQuantity"));
		elementClick(cartPage.proceedToCheckoutCartSummary, "Proceed to Checkout", "Cart Summary Page");

		//Sign in Tab
		enterTextValue(loginPage.emailAddressSignin, "Email Address Field Signin", "Login Page", randomString(5) + testData.get("emailDomain"));
		elementClick(loginPage.createAccountButton, "Create Account Button", "Login Page");
		wait.until(ExpectedConditions.visibilityOf(signupPage.personalInfoHeader));
		enterTextValue(signupPage.persoalInfoMandatoryTextFields.get(0), "First Name Field", "Sign up Page", testData.get("FirstName"));
		enterTextValue(signupPage.persoalInfoMandatoryTextFields.get(1), "Last Name Field", "Sign up Page", testData.get("LastName"));
		enterTextValue(signupPage.persoalInfoMandatoryTextFields.get(3), "Password Field", "Sign up Page", testData.get("Password"));
		enterTextValue(signupPage.addressInfoMandatoryTextFields.get(0), "First Name Field", "Sign up Page", testData.get("FirstName"));
		enterTextValue(signupPage.addressInfoMandatoryTextFields.get(1), "Last Name Field", "Sign up Page", testData.get("LastName"));
		enterTextValue(signupPage.addressInfoMandatoryTextFields.get(2), "Address Field", "Sign up Page", testData.get("Address"));
		enterTextValue(signupPage.addressInfoMandatoryTextFields.get(3), "City Field", "Sign up Page", testData.get("City"));
		selectValueFromDropdown(signupPage.addressInfoMandatoryDropdowns.get(0), "State Dropdown", "Sign up Page", testData.get("State"));
		enterTextValue(signupPage.addressInfoMandatoryTextFields.get(4), "Zip Code Field", "Sign up Page", testData.get("ZipCode"));
		enterTextValue(signupPage.addressInfoMandatoryTextFields.get(5), "Mobile Number Field", "Sign up Page", testData.get("Mobile"));
		enterTextValue(signupPage.addressInfoMandatoryTextFields.get(6), "Reference Mail Id Field", "Sign up Page", randomString(5)+testData.get("emailDomain"));
		elementClick(signupPage.registerButton, "Register Button", "Sign up Page");
		elementClick(cartPage.processAddressButton, "Process Address Button", "Cart Page Address Tab");
		jsClick(cartPage.termsConditionsCheckbox);
		elementClick(cartPage.proceedToCheckoutShippingTab, "Proceed to Checkout", "Cart Page Shipping Tab");
		elementClick(cartPage.paymentByBankWire, "Pay by Bank Wire", "Cart Page Payment Tab");
		elementClick(cartPage.confirmOrderButton, "Confirm Order Button", "Cart Page Payment Tab");
		String paidAmount = cartPage.paidAmount.getText();
		String confirmationMessage = cartPage.confirmationMessage.getText();
		elementClick(cartPage.viewCustomerAccountLink, "View Customer Account Link", "Cart Page");
		elementClick(cartPage.orderHistoryDetailsButton, "Order History & Details Button", "View Account Page");
		Map<String, Integer> orderHistoryTableHeaderTexts = getColIndex(cartPage.orderHistoryTableHeaders);
		List<WebElement> orderReferenceNumbers = cartPage.replacedOrderReferenceNumberXpath(driver, Integer.toString(orderHistoryTableHeaderTexts.get("Order reference")));
		int orderReferenceRowIndex = 0;
		for (WebElement x : orderReferenceNumbers) 
		{
			if (confirmationMessage.contains(x.getText()))
				orderReferenceRowIndex = getRowIndex(orderReferenceNumbers, x.getText());
		}
		String amountOrderHistoryTable = cartPage.getAmountFromOrderHistoryTable(driver, Integer.toString(orderReferenceRowIndex), Integer.toString(orderHistoryTableHeaderTexts.get("Total price")))
												 .getText();
		Assert.assertEquals(amountOrderHistoryTable, paidAmount);
		elementClick(cartPage.logoutButton, "Logout Button", "Cart Page");
	}

}
