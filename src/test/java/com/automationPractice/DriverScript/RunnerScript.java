package com.automationPractice.DriverScript;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automationPractice.BusinessLib.PlaceOrderScenario;
import com.automationPractice.TestBase.BaseClass;


public class RunnerScript extends BaseClass
{
	PlaceOrderScenario placeOrder;
	JSONParser jsonParser;
	JSONObject jsonObject;
	
	@BeforeClass
	public void initObjects() throws FileNotFoundException, IOException, ParseException
	{
		placeOrder = new PlaceOrderScenario(driver);
		String jsonFilePath = ".\\src\\main\\resources\\TestData.JSON";
		jsonParser = new JSONParser();
		jsonObject = (JSONObject)jsonParser.parse(new FileReader(jsonFilePath));
	}
	
	@Test
	public void automationScenario() throws Exception
	{
		Map<String, String> testDataOrderPlace = (Map<String, String>) jsonObject.get("Order Place");
		placeOrder.placeOrderScenario(testDataOrderPlace);
	}

}
